# Payment Date tool

A PHP Command-line utility that gives the output a CSV file, containing the payment dates for the remainder of this year.
The payment date depends on the following conditions:

- Sales staff gets a monthly fixed base salary and a monthly bonus.
- The base salaries are paid on the last day of the month unless that day is a Saturday or a Sunday (weekend).
- On the 15th of every month bonuses are paid for the previous month, unless that day is a weekend. In that case, they are paid the first Wednesday after the 15th.

## Getting Started
You can download project and run project using following instructions.

### Prerequisities
- [Composer](https://getcomposer.org/)
- [PHP 7](http://www.php.net/)

### Installing

Clone the repository
```
git clone <HTTPS URL> of repository
```

Download dependancies
```
composer install
```


### Run application

```
php index.php <filename>
```
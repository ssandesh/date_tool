<?php

namespace BAS\DateTool\Libraries;

/**
 * Represents a simple date interface
 */

interface IDates
{
    /**
	* Get a formatted date from date
	* @param $date of type date
	* @return date| string
	*/
	public function getDate( \DateTime $date ): string;
}

?>
<?php

namespace BAS\DateTool\Libraries;

/**
 * Using to handle all types of file operation.
 */
 
trait TFileOperations {
	
	/**
	* Validate the file name
	* @param $strFileName| String
	* @return result| bool
	*/
	function isValidFileName( $strFileName ) {
		if( preg_match( '/^([-\.\w]+)$/', $strFileName ) ) {
			return true;
		}
		return false;
	}
    
	/**
	* Get a CSV file with help of array data
	* @param $arrData| array
	* @param $strFileName| String
	* @return result| bool
	*/
	public function generateCSV( array $arrData, string $strFileName ): bool {

		if( false == is_array( $arrData ) ) {
			return false;
		}

		$directoryPath = PROJECT_ROOT . DIRECTORY_SEPARATOR . 'downloads/';

		if ( false == file_exists( $directoryPath ) ) {
			mkdir( $directoryPath, 0777, true);
		}
		
		$objFile = fopen( $directoryPath . $strFileName . '.csv', 'wb' );
		
		foreach( $arrData as $arrVal ){
			fputcsv( $objFile, $arrVal );
		}
		fclose( $objFile );
		return true;
    }
}

?>
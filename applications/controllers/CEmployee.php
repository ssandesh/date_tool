<?php

namespace BAS\DateTool\Controllers;

use BAS\DateTool\Controllers\{ CPayment, CBonus };
use BAS\DateTool\Libraries\TFileOperations;

/**
 * Using to handle all the operations related to the employee.
 */

class CEmployee extends CPayment {
	
	use TFileOperations;
	
	/**
	* Generate a CSV contains the payment and bonus date
	* @param $strFileName| string 
	* @return result| string
	*/
	public function getPaymentBonusDates( string $strFileName ): bool {
		
		$arrData	= [];
		$arrData[]	= [ 'Month Name', 'Salary Payment Date', 'Bonus Payment Date' ];
		
		$date = new \DateTime();
		$intMonth = $date->format( 'n' );
		
		$objBonus = new CBonus();
		
		for( ; $intMonth <= 12; $intMonth++ ) {
			$date->setDate( $date->format( 'Y' ), $intMonth, 1 );

			$arrData[$intMonth]		= [];
			$arrData[$intMonth][]	= $date->format( 'F' );
			$arrData[$intMonth][]	= $this->getDate( $date );
			$arrData[$intMonth][]	= $objBonus->getDate( $date );
		}
		return $this->generateCSV( $arrData, $strFileName );
	}

}

?>

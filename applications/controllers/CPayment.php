<?php

namespace BAS\DateTool\Controllers;

use BAS\DateTool\Libraries\IDates;

/**
 * Using to handle all the operations related to the payment.
 */

class CPayment implements IDates {

	/**
	* Get a bonus date by date
	* Deafult value: last day of every month and not weekend.
	* @param $date of type date
	* @return date| string
	*/
	public function getDate( \DateTime $date ): string {

		$date->setDate( $date->format( 'Y' ), $date->format( 'm' ), $date->format( 't' ) );
		$strLastMonthdate  = $date->format( 'Y-m-t' );
		$strLastWorkingday = $date->format( 'l' );

		if( $strLastWorkingday == "Saturday" ) { 
			$date->modify( "-1 day" );
			$strLastMonthdate = $date->format( 'Y-m-d' );
		} elseif( $strLastWorkingday == "Sunday" ) {
			$date->modify( "-2 day" );
			$strLastMonthdate = $date->format( 'Y-m-d' );
		}
		return $strLastMonthdate;
	}

}

?>

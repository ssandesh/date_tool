<?php 

namespace BAS\DateTool\Controllers;

use BAS\DateTool\Libraries\IDates;

/**
 * Using to handle all the operations related to the bonus.
 */

class CBonus implements IDates {
	
	/**
	* Get a bonus date by date
	* Deafult value: 15th of every month if not weekend else next the first Wednesday after the 15th.
	* @param $date of type date
	* @return date| string
	*/
	public function getDate( \DateTime $date ): string {

		$date->setDate( $date->format( 'Y' ), $date->format( 'm' ), 15 );
		$strWeekDay = $date->format('l');
		
		if($strWeekDay == "Saturday" || $strWeekDay == "Sunday" ) { 
			$date->modify('next wednesday');
		}
		return $date->format('Y-m-d');;
	}
}

?>
<?php

require_once __DIR__ . '/vendor/autoload.php';

use BAS\DateTool\Controllers\CEmployee;

define('PROJECT_ROOT', __DIR__);

$objEmployee = new CEmployee();

if( true == isset( $argv ) && 2 > count( $argv ) ) {
	echo "Please provide file name as argument.\n";
	exit;
} elseif( true == isset( $argv[1] ) && false == $objEmployee->isValidFileName( $argv[1] ) ) {
	echo 'Incorrect filename Found.';
	exit;
}
$strFileName = $argv[1];

if( true == file_exists( PROJECT_ROOT . '/downloads/' . $strFileName . '.csv' ) ) {
	echo 'File ' . $strFileName . ' is already exists. Please provide different filename.';
	exit;
}

if( true == $objEmployee->getPaymentBonusDates( $strFileName ) ) {
	echo "File generated successfully in 'downloads' directory";
} else {
	echo 'Something wents wrong';
}

?>